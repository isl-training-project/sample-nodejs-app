const express = require('express')
const app = express()
const port = process.argv[2] || 3000
const node_env = process.argv[3] || "developement"

app.get('/', (req, res) => {
  // res.send(`<h1><center>Hello From ISL Academy!</center></h1>\n<h2><center>APPID = ${node_env}, PORT = ${port}</center></h2>`)
  res.send(`<h1 style="background-color: coral;"><center>Hello From ISL Academy!</center></h1>\n<h2 style="background-color: coral;"><center>APPID = ${node_env}, PORT = ${port}</center></h2>`)
})

app.get('/admin', (req, res) => {
  // res.send(`<h1><center>Hello From ISL Academy Admin!</center></h1>\n<h2><center>APPID = ${node_env}, PORT = ${port}</center></h2>`)
  res.send(`<h1 style="background-color: coral;"><center>Hello From ISL Academy Admin!</center></h1>\n<h2 style="background-color: coral;"><center>APPID = ${node_env}, PORT = ${port}</center></h2>`)
})

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}/`)
})
